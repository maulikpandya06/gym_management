from django.core import validators
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

from django.contrib.admin.widgets import AdminDateWidget

from .models import Member, Plans

# class AdminRegister(forms.ModelForm):
      # def __init__(self, *args, **kwargs):
      #   super().__init__(*args, **kwargs)

      #   for field in self.Meta.required:
      #       self.fields[field].required = True

#       class Meta:
#             model = User
#             fields = ['email', 'password']
            # error_messages = {'email':{'required':'Email is required'},
            #                   'passowrd':{'required':'Password is required'}
            #                   ,}
#             widgets = {'email': forms.EmailInput, 'password':forms.PasswordInput}
#             required = ('email', 'password')

class AdminSignupForm(UserCreationForm):

      email     = forms.CharField(required=True, widget=forms.EmailInput(attrs={'class':"form-control"}))
      username  = forms.CharField(required=True, label='Username', widget=forms.TextInput(attrs={'class':'form-control'}))
      password1 = forms.CharField(required=True,label='Password', widget=forms.PasswordInput(attrs={'class':"form-control"}))
      password2 = forms.CharField(required=True,label='Confirm Password', widget=forms.PasswordInput(attrs={'class':"form-control"}))

      class Meta:
            model  = User
            fields = ['username', 'email', 'password1', 'password2']
            error_messages = {'email':{'required':'Email is required'},
                  'passowrd':{'required':'Password is required'}
                  ,}
            labels = {'email':'Email', 'password1':'Password'}


class AddMemberForm(forms.ModelForm):
      

      first_name  = forms.CharField(widget=forms.TextInput(attrs={'class':"form-control"}))
      last_name   = forms.CharField(widget=forms.TextInput(attrs={'class':"form-control"}))
      city        = forms.CharField(widget=forms.TextInput(attrs={'class':"form-control"}))
      zipcode     = forms.IntegerField(widget=forms.TextInput(attrs={'class':"form-control"}))
      phoneno     = forms.IntegerField(widget=forms.TextInput(attrs={'class':"form-control"}))
      bought_on   = forms.DateField(widget=forms.DateInput(attrs={'type':'date','name':'bought_on'}))
      # plan        = forms.ChoiceField(widget=forms.Select(attrs={'class':"custom-select"}))
      plan      = forms.ModelChoiceField(queryset=Plans.objects.all(), widget=forms.Select(attrs={'name':'plan_id'}))

      class Meta:
            model   = Member
            fields  = ['first_name', 'last_name', 'city', 'zipcode', 'phoneno', 'bought_on', 'plan']
            labels  = {'first_name':'First Name', 'last_name': 'Last Name', 'city':'City', 'zipcode':'Zipcode', 'plan':'Plans'}
            # widgets = {'plan': forms.}

      

class AddPlan(forms.ModelForm):
      plan_name    = forms.CharField(widget=forms.TextInput(attrs={'class':"form-control"}))
      description  = forms.CharField(widget=forms.Textarea(attrs={'class':"form-control"}))
      price        = forms.IntegerField(widget=forms.NumberInput(attrs={'class':"form-control"}))
      duration     = forms.IntegerField(widget=forms.NumberInput(attrs={'class':"form-control"}))
      class Meta:
            model = Plans
            fields = ['plan_name', 'description', 'price', 'duration']
            lables = {'plan_name':'Plan Name', 'description':'Description', 'price':'Price', 'duration':'Duration'}
