from django.shortcuts import render, redirect
from django.contrib import messages
from django.db import transaction
# from django.contrib.auth.forms import UserCreationForm
from .forms import AdminSignupForm, AddMemberForm, AddPlan
from .models import Member, Plans
from django.contrib.auth.decorators import login_required

from django.contrib.auth.models import User
from django.contrib.auth import login, logout
from django.contrib.auth.hashers import check_password


##############  REGISTER ADMIN  ##################
@transaction.atomic
def registerAdmin(request):
      if request.method == 'POST':
            form = AdminSignupForm(request.POST)
            if form.is_valid():
                  messages.success(request, 'Account Created!!')
                  # User.objects.create(email=request.POST.get('email'), password=request.POST.get('password'))
                  form.save()

                  context = {'form':form}
                  return redirect('gym_app:loginAdmin', permanent=True)
            else:
                  context = {'form': form}
                  return render(request, 'gym_app/pages/registerAdmin.html', context)
      else:
            form = AdminSignupForm()
            context = {'form':form}
            return render(request, 'gym_app/pages/registerAdmin.html', context)


##############  LOGIN ADMIN  ########################
@transaction.atomic
def loginAdmin(request):
      
      if request.user.is_authenticated:
            return redirect('gym_app:home', permanent=True)

      if request.method == 'POST':
            email = request.POST.get('email')
            password = request.POST.get('password')
            # user = authenticate(email=email, password=password)
            try:
                  user = User.objects.get(email=email)
            except User.DoesNotExist:
                  return render(request, 'gym_app/pages/loginAdmin.html', context={'message':'Invalid Credentials.'})
            if check_password(password, user.password):
                  if user.is_active:
                        login(request, user)
                        return redirect('gym_app:home', permanent=True)
            return render(request, 'gym_app/pages/loginAdmin.html', context={'message':'Invalid Credentials.'})
      return render(request, 'gym_app/pages/loginAdmin.html')


##############  HomePage  ######################
@login_required(login_url="/")
def home(request):
      if request.method == 'GET':
            members = Member.objects.all()
            context = {'members':members}

            return render(request, 'gym_app/pages/home.html', context)

##############  Adding a new member  ####################
@login_required(login_url="/")
@transaction.atomic
def addMember(request):
      if request.method == 'GET':
            fm = AddMemberForm()
            plans = Plans.objects.all()
            context = {'title': 'Add Member', 'form':fm, 'plans':plans, 'button':'Add Member'}
            return render(request, 'gym_app/pages/addMember.html', context)

      elif request.method == 'POST':
            fm = AddMemberForm(request.POST)

            if fm.is_valid():
                  fm.save()
                  return redirect('gym_app:home', permanent=True)
            # return render(request, 'gym_app/pages/addMember.html')
            return redirect('gym_app:addMember', permanent=True)


##############  Updating a member  ####################
@login_required(login_url="/")
@transaction.atomic
def updateMember(request, pk):
      
      member = Member.objects.filter(member_id=pk).first()
      fm = AddMemberForm(instance=member)
      context = {'form':fm, 'button':'Update Member'}
            
      if request.method == 'POST':
            fm = AddMemberForm(request.POST, instance=member)
            
            if fm.is_valid():
                  fm.save()
                  return redirect('gym_app:home', permanent=True)
      return render(request, 'gym_app/pages/addMember.html', context)


##############  Deleting a member  ####################
@login_required(login_url="/")
def deleteMember(request, pk):
      member = Member.objects.filter(member_id=pk).first()
      if member is not None:
            member.delete()
      return redirect('gym_app:home', permanent=True)


##############  List of All Membership Plans  ####################
@login_required(login_url="/")
def plans(request):
      plans = Plans.objects.all()
      context = {'title': 'Add Member', 'plans':plans}
      return render(request, 'gym_app/pages/plans.html', context)


##############  Adding a New Membership Plan  #######################
@login_required(login_url="/")
@transaction.atomic
def addPlan(request):

      fm = AddPlan()
      context = {'title': 'Add Plan', 'form':fm}
      
      
      if request.method == 'POST':
            plan = AddPlan(request.POST)
            if plan.is_valid():
                  plan.save()
                  
                  return redirect('gym_app:plans', permanent=True)
      return render(request, 'gym_app/pages/addPlan.html', context)


##############  Updating a Membership Plan  ####################
@login_required(login_url="/")
@transaction.atomic
def updatePlan(request, pk):
      plan = Plans.objects.filter(plan_id=pk).first()
      fm = AddPlan(instance=plan)
      context = {'form':fm, 'button':'Update Member'}
      if request.method == 'POST':
            fm = AddPlan(request.POST, instance=plan)

            if fm.is_valid():
                  fm.save()
                  return redirect('gym_app:plans', permanent=True)
      return render(request, 'gym_app/pages/addPlan.html', context)


##############  Delete a Membership Plan  ####################
@login_required(login_url="/")
@transaction.atomic
def deletePlan(request, pk):
      plans = Plans.objects.filter(plan_id=pk).first()
      if plans is not None:
            plans.delete()
      return redirect('gym_app:plans', permanent=True)

##############  Logout  ########################
@login_required(login_url="/")
@transaction.atomic
def logoutAdmin(request):
      logout(request)
      return redirect('gym_app:loginAdmin', permanent=True)

