from django.db import models
from django.contrib.auth.models import User
import datetime as dt
from django.utils import timezone 
from datetime import datetime, timedelta
# Create your models here.

# User.add_to_class(
#       'email', models.EmailField(null=False,blank=False)
# )

class Plans(models.Model):
      plan_id     = models.AutoField(primary_key=True)
      plan_name   = models.CharField(max_length=255, blank=True, null=True)
      description = models.TextField(blank=True, null=True)
      price       = models.IntegerField(blank=True, null=True)
      duration    = models.IntegerField(blank=True, null=True)

      def __str__(self) -> str:
            return self.plan_name
      class Meta:
            app_label = 'gym_app'

class Member(models.Model):
      member_id   = models.AutoField(primary_key=True)
      first_name  = models.CharField(max_length=255, blank=True, null=True)
      last_name   = models.CharField(max_length=255, blank=True, null=True)
      city        = models.CharField(max_length=100, blank=True, null=True)
      zipcode     = models.IntegerField(blank=True, null=True)
      phoneno     = models.IntegerField(blank=True, null=True)
      bought_on   = models.DateField(blank=True, null=True)
      # plan        = models.ForeignKey(Plans, on_delete=models.PROTECT)
      plan        = models.ForeignKey(Plans, null=True, blank=True, on_delete=models.SET_NULL)

      @property
      def expires_in(self):

            bought = self.bought_on
            try:
                  duration = int(self.plan.duration)
            except Exception as e:
                  return "PLAN DELETED"
            # date = dt.date(2023, 1, 12)
            # delta = bought - date
            delta = bought - datetime.now().date()
            days_left = duration + delta.days
            return days_left

      class Meta:
            app_label = 'gym_app'