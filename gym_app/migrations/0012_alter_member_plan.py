# Generated by Django 4.1.3 on 2022-12-08 19:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("gym_app", "0011_alter_member_bought_on"),
    ]

    operations = [
        migrations.AlterField(
            model_name="member",
            name="plan",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                to="gym_app.plans",
            ),
        ),
    ]
