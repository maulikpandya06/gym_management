from django.contrib import admin
from django.urls import path

from django.urls import path
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
      path('registerAdmin', registerAdmin, name='registerAdmin'),
      path('', loginAdmin, name='loginAdmin'),
      path('home', home, name='home'),
      path('logout', logoutAdmin, name='logout'),
      path('addMember', addMember, name='addMember'),
      path('updateMember/<str:pk>', updateMember, name='updateMember'),
      path('deleteMember/<str:pk>', deleteMember, name='deleteMember'),
      path('plans', plans, name='plans'),
      path('addPlan', addPlan, name='addPlan'),
      path('updatePlan/<str:pk>', updatePlan, name='updatePlan'),
      path('deletePlan/<str:pk>', deletePlan, name='deletePlan'),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

app_name = 'gym_app'